
CFLAGS	= -g -Wall -DSUN
# CFLAGS	= -g -Wall -DDEC
CC	= gcc
CCF	= $(CC) $(CFLAGS)

H	= .
C_DIR	= .

INCDIR	= -I$(H)
LIBDIRS = -L$(C_DIR)
LIBS    = -lclientReplFs

CLIENT_OBJECTS = client.o

all:	appl server

appl:	appl.o $(C_DIR)/libclientReplFs.a 
	$(CCF) -o appl appl.o $(LIBDIRS) $(LIBS)

appl.o:	appl.c client.h appl.h
	$(CCF) -c $(INCDIR) appl.c

$(C_DIR)/libclientReplFs.a:	$(CLIENT_OBJECTS) network.o
	ar cr libclientReplFs.a $(CLIENT_OBJECTS) network.o
	ranlib libclientReplFs.a

client.o:	client.c client.h network.h
	$(CCF) -c $(INCDIR) client.c

server: server.o network.o
	$(CCF) -o replFsServer server.o network.o
	
server.o: server.c server.h network.h
	$(CCF) -c $(INCDIR) server.c

network.o: network.c network.h
	$(CCF) -c $(INCDIR) network.c
		
clean:
	rm -f appl replFsServer *.o *.a

