/*
 * server.c
 *
 *  Created on: May 15, 2013
 *      Author: jessho
 */


//#define DEBUG true

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>

#include <server.h>
#include <network.h>

// Some data structure definitions
typedef	struct {
	int commit;				// write update commit #
	int index;				// write update sequence number
	char filename[MAXNAME];	// file name for write update
	char block[MAXBLOCK+1];	// content of the write update
	int offset;				// offset into file for writing
	int blockSize;			// size of the block
} wupdate;

// Some constants
int NormalReturn = 0;
int ErrorReturn = -1;
const char *closedFile = "/";
bool BRAND_NEW = false;

// These variables are set by command-line arguments
int port = 0; 	// The port number to pass to netInit().
char * mount;	// The file path for the mounted directory.
int drop = 0;	// The packet drop percentage.

// These variables are used in communication over the network
char myUID[37];	// This server's UUID (32 hex digits + 4 hyphens + null terminator)
int myStatus;		// Sequence number for status messages

// These variables are for keeping track of writes
int myfd;			// File descriptor of file in which we are writing
int _myfd;		// File descriptor of the backup to the file we're writing in
char myFilename[MAXNAME];	// Name of the file we're writing to
char _myFilename[MAXNAME + 1];
int myCommits;		// Counter of commits (monotonically increases after completed and rollback transactions)
int myWrites;		// Counter of writes
int lastWrite;		// Counter for last write to be included in a commit.
bool commitMode;
bool votingMode;
wupdate transaction[MAXWRITES];	// An array of pointers to the write updates we want to commit in a transaction


/* Create a UUID compliant with version 4. A UUID is 32 hex digits separated
 * into groups with the pattern "8-4-4-4-12". Writes to the character buffer
 * provided as an argument. Ex: df03b568-e554-46ba-8be9-9aab8e58848
 */
void generateUID(char * out)
{
	sprintf(out, "%04x%04x-%04x-%04x-%04x-%04x%04x%04x",
	    (unsigned short)rand(), (unsigned short)rand(),                 // Generates a 64-bit Hex number
	    (unsigned short)rand(),                         // Generates a 32-bit Hex number
	    ((rand() & 0x0fff) | 0x4000),   // Generates a 32-bit Hex number of the form 4xxx (4 indicates the UUID version)
	    rand() % 0x3fff + 0x8000,       // Generates a 32-bit Hex number in the range [0x8000, 0xbfff]
	    (unsigned short)rand(), (unsigned short)rand(), (unsigned short)rand());        // Generates a 96-bit Hex number
	printf("INFO: assigned UID %s\n", out);
}


/*
 * Initializes local data structures ands fields, such as generating UUID,
 * message sequence numbers, transaction buffers, etc.
 */
void localInit() {
	// seed the random number generator
	struct timeval now;
	gettimeofday(&now, 0);
	srand(now.tv_sec + now.tv_usec);
//	printf("%d\n", rand());

	memset(myUID, 0, sizeof(myUID));
	generateUID(myUID);

	// zero all local data structures
	int i;
	myStatus = 0;
	myCommits = 0;
	myWrites = 0;
	myfd = 0;
	_myfd = 0;
	commitMode = false;
	votingMode = false;
	lastWrite = MAXWRITES;
	strncpy(myFilename, closedFile, strlen(closedFile) + 1);
	for (i = 0; i < MAXWRITES; i++ ) {
		memset(&transaction[i], 0, sizeof(wupdate));
	}
}

/*
 * Given a directory path, we check if the path is available and create it.
 * If the directory is in use (exists), then we exit with a message.
 */
void directoryInit(char * path) {
	struct stat st = {0};
	char command[128];
	if(stat(path, &st) == -1) {
		// if directory does not yet exist
		sprintf(command, "mkdir -p %s", path);
		system(command);
	} else {
		printf("Machine already in use!\n");
		exit(1);
	}
	// change directory into specified mount.
	if(chdir(path) == 0) {
		printf("INFO: mounted directory '%s'\n", path);
	}
}

/*
 * Given a packet, interprets it and routes control flow if a reaction
 * is necessary
 */
void processPacket(char * packet) {
	// temporarily add a delay to test that the port is set up properly
	//usleep(500000);

	if (rand()%100 < drop) return;
	int c = atoi(&packet[0]);
	switch(c) {
	case 1:	// status message from servers
//		printf("GOT status: %s\n", packet);
		break;
	case 2:	// init message from client
		printf("GOT init: %s\n", packet);
		processInitMessage(packet);
		break;
	case 3:	// open message from client
//		printf("GOT open: %s\n", packet);
		processOpenMessage(packet);
		break;
	case 4:	// write message from client
//		printf("GOT write: %s\n", packet);
		processWriteMessage(packet);
		break;
	case 5:	// poll message from client
//		printf("GOT poll: %s\n", packet);
		processPollMessage(packet);
		break;
	case 6:	// vote message from server
//		printf("GOT vote: %s\n", packet);
		break;
	case 7:	// complete message from client
//		printf("GOT complete: %s\n", packet);
		processCompleteMessage(packet);
		break;
	case 8:	// done (ACK) from server
//		printf("GOT done: %s\n", packet);
//		processDoneMessage(packet);
		break;
	case 9:	// close message from client
//		printf("GOT close: %s\n", packet);
		processCloseMessage(packet);
		break;
	case 10:	// exit message from server
		break;
	default:
		break;
	}
}

/*
 * When a server sees an init message, it knows a new client is joining the
 * network. Responds by sending a status message.
 */
void processInitMessage(char *packet) {
	// zero local data structures
	int i;
	myCommits = 0;
	myWrites = 0;
	commitMode = false;
	votingMode = false;
	lastWrite = MAXWRITES;
	strncpy(myFilename, closedFile, strlen(closedFile) + 1);
	for (i = 0; i < MAXWRITES; i++ ) {
		memset(&transaction[i], 0, sizeof(wupdate));
	}

	sendStatusMessage();
}

void sendStatusMessage() {
	// 1_<Server ID>_<status seq #>_<commit seq #>_<write seq #>_<file name>
	char out[512];
	int bytes;
	sprintf(out, "1_%s_%d_%d_%d_%s", myUID, myStatus, myCommits, myWrites, myFilename);

	if((bytes = sendto(mySocket, out, strlen(out), 0, (struct sockaddr *) &groupAddr, sizeof(Sockaddr))) < 0) {
		perror("sendto\n");
	} else {
		myStatus++;
		printf("SENT status: %s\n", out);
	}
}

/*
 * When a server sees an open message, it knows the client is trying
 * to open a file. Server tries to open the file, and set its local data
 * structures properly.
 */
void processOpenMessage(char * packet){
	int c;
	int w;
	char *f;

	// 	3_<commit seq #>_<write seq #>_<file name>
	strtok(packet, "_");			// strip event type
	c = atoi(strtok(NULL, "_"));	// get commit seq number
	w = atoi(strtok(NULL, "_"));	// get write seq number
	f = strtok(NULL, "_");			// get the file name

	// error check
	if( c < myCommits ) {				// ignore old messages
		return;
	}
	if(strcmp(myFilename, f) == 0) {	// we've already opened this file
		sendStatusMessage();
		return;
	}
	if(strcmp(myFilename, closedFile) != 0){	// we're working on another file
		printf("Cannot write to multiple files");
		return;
	}

	if( access( f, F_OK ) != -1 ) {
	    // file exists
		BRAND_NEW = false;
	} else {
		// file does not yet exist
	    BRAND_NEW = true;
	}

	// make a backup of the original. set working version to filename~
	myfd = open( f, O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR );	// original
	if ( myfd < 0) {
		perror( "OpenFile" );
		return;
	}

	char command[300];
	sprintf(_myFilename, "%s~", f);
	sprintf(command, "\\cp ./%s ./%s", f, _myFilename);
	system(command);

	// close original
	close(myfd);

	_myfd = open( _myFilename, O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR );	// working version
	if ( _myfd < 0) {
		perror( "OpenFile" );
		return;
	}

	// finally, set local data
	myCommits = c;
	myWrites = w;	// should be 0 on new file open
	sprintf(myFilename, "%s", f);	// set myFilename after error checks

	sendStatusMessage();
}

/*
 * When client sends a staged write, we add it to the transaction array,
 * which holds all staged writes (in order of execution).
 */
void updateHistory(int commit, int write, char * name, char * block, int offset, int blockSize ){
	int i = write - 1;					// writes are 1-indexed, whereas transacition[] is 0-indexed
	if(transaction[i].index != 0) {		// check for empty spot
		return;
	}
	transaction[i].commit = commit;
	transaction[i].index = write;
	sprintf(transaction[i].filename, "%s", name);
	sprintf(transaction[i].block, "%s", block);
	transaction[i].offset = offset;
	transaction[i].blockSize = blockSize;

//	printf("Updated transaction. commit:%d, index:%d, blockSize:%d, offset:%d, block:%s, file:%s\n",
//			transaction[i].commit, transaction[i].index, transaction[i].blockSize,
//			transaction[i].offset, transaction[i].block, transaction[i].filename);
}

/*
 * Checks the transaction history to see if we can try to execute any
 * more staged writes (no missing execution instructions)
 */
void tryWrite() {
	if(myWrites >= 128) {
		perror("TryWrite. Staged writes capped to 128 between commits.\n");
		return;
	}

	int i = myWrites;
	wupdate w;
	while(i < MAXWRITES && transaction[i].index != 0 ) {
		w = transaction[i];
		if( lseek(_myfd, w.offset, SEEK_SET) < 0 ) {
			perror("TryWrite seek\n");
		}
		if( write(_myfd, w.block, w.blockSize) < 0) {
			perror("TryWrite write\n");
		}
		i++;
	}
	myWrites = i;
//	printf("tryWrite got to %d\n", myWrites);

	if(votingMode) {	// optimistically ask for next missing piece
		if(myWrites < lastWrite)
			sendStatusMessage();
		else if (myWrites == lastWrite) {
			sendVoteMessage("YES");
		}
	}
}
/*
 * When client sends staged write, we add to transaction history, and try
 * to execute write updates if we aren't missing contiguous executions.
 */
void processWriteMessage(char * packet) {
	int c;
	int w;
	int bsize;
	int o;
	char b[MAXBLOCK + 1];

//	4_<commit seq #>_<write seq #>_<block size>_<offset>_<write block>
	strtok(packet, "_");			// strip event type
	c = atoi(strtok(NULL, "_"));	// get commit seq number
	if( c < myCommits) {			// check if this is an old commit
		return;
	}

	w = atoi(strtok(NULL, "_"));	// get write seq number
	if( w > MAXWRITES ) {			// chekc if we've exceeded max writes between commits
		return;
	}
	if(transaction[w-1].index != 0) {	// check for redundant information
		return;
	}

	bsize = atoi(strtok(NULL, "_"));	// get block size
	if( bsize > MAXBLOCK + 1) {		// check if we've exceed max block size
		return;
	}

	o = atoi(strtok(NULL, "_"));	// get write offset
	sprintf(b, "%s", strtok(NULL, "_"));			// get the write block

	updateHistory(c, w, myFilename, b, o, bsize);	// add to write history
	tryWrite();										// try performing the write

}

/*
 * Client sent a poll request. Server votes YES if ready to finish transaction,
 * otherwise sends a request for missing information (via status message).
 */
void processPollMessage(char * packet) {
	int c;

//	5_<commit seq #>_<write seq #>
	strtok(packet, "_");			// strip event type
	c = atoi(strtok(NULL, "_"));	// get commit seq number
	lastWrite = atoi(strtok(NULL, "_"));	// get write seq number

	if( c != myCommits ) {			// ignore old messages
		return;
	}


	votingMode = true;

	if( myWrites == lastWrite ) {
		sendVoteMessage("YES");
	} else if (myWrites < lastWrite) {	// blocked by missing write transaction
		sendStatusMessage();
	} else {
		sendVoteMessage("NO");
	}
}

/*
 * In response to a poll message, server sends a VOTE message
 */
void sendVoteMessage(char *response) {

//	6_<server UID>_<commit seq #>_<write seq #>_<vote>
	char vote[512];
	int bytes;
	sprintf(vote, "6_%s_%d_%d_%s", myUID, myCommits, myWrites, response);

	if((bytes = sendto(mySocket, vote, strlen(vote), 0, (struct sockaddr *) &groupAddr, sizeof(Sockaddr))) < 0) {
		perror("sendto\n");
	} else {
		myStatus++;
		printf("SENT vote: %s\n", vote);
	}
}

/*
 * Takes care of copying or rolling back the working file version to the
 * backup version.
 */
void localCommit() {
	char command[300];
	if (commitMode) {
		// commit locally by copying working to original file
		sprintf(command, "\\cp ./%s ./%s", _myFilename, myFilename);
		system(command);
		BRAND_NEW = false;
	} else {
		// rollback by coping original to working file
		sprintf(command, "\\cp ./%s ./%s", myFilename, _myFilename);
		system(command);
	}
}

/*
 * In phase 2 of committing, client (coordinator) tells servers whether
 * to finish comitting or to rollback. Server sends ACK in response when
 * finished.
 */
void processCompleteMessage(char * packet) {
	int c;
	char * command;

//	7_<Commit #>_<complete>
	strtok(packet, "_");			// strip event type
	c = atoi(strtok(NULL, "_"));	// get commit seq number
	command = strtok(NULL, "_");	// get the COMMIT/ROLLBACK command

	if( c == myCommits ) {			// ignore old messages
		if( strcmp(command, "COMMIT") == 0) {
			commitMode = true;
		} else {
			commitMode = false;
		}

		localCommit();					// save the changes, or rollback file to backup
		cleanupAfterCommit();
	}

	sendDoneMessage();
}

/*
 * After a commit/rollback, update counters and reset local data structures.
 */
void cleanupAfterCommit() {
	myCommits++; // update local data
	myWrites = 0;
	memset(transaction, 0, sizeof(transaction));	// wipe write history
	commitMode = false;
	votingMode = false;
	lastWrite = MAXWRITES;

	printf("Cleaned after commit. Commit:%d, Writes:%d\n", myCommits, myWrites);
}

/*
 * ACK sent to client in response to COMPLETE message
 */
void sendDoneMessage() {
	//	8_<Server UUID>_<Commit #>
	char done[64];
	int bytes;
	sprintf(done, "8_%s_%d", myUID, myCommits);

	if((bytes = sendto(mySocket, done, strlen(done), 0, (struct sockaddr *) &groupAddr, sizeof(Sockaddr))) < 0) {
		perror("sendto\n");
	} else {
		myStatus++;
		printf("SENT done: %s\n", done);
	}
}

/*
 * Doing file closing
 */
void processCloseMessage(char * packet) {
	int c;
	int w;
	char * f;

	//	9_<commit seq #>_<transaction seq #>_<file name>
	strtok(packet, "_");			// strip event type
	c = atoi(strtok(NULL, "_"));	// get commit seq number
	w = atoi(strtok(NULL, "_"));	// get write seq number
	f = strtok(NULL, "_");	// get the COMMIT/ROLLBACK command

	if( c < myCommits ) {			// ignore old messages
		return;
	}
	if( w != myWrites ) {
		perror("Close file writes mismatch with local writes.\n");
	}
	if(strcmp(myFilename, closedFile) == 0) {	// file's already closed.
		sendStatusMessage();
		return;
	}
	if(strcmp(myFilename, f) == 0) {
		if ( close( _myfd ) < 0 ) {
			perror("Close");
			return;
		}

		char command[300];
		sprintf(command, "\\rm ./%s", _myFilename);	// remove working file
		system(command);

		if(BRAND_NEW) {		// if we never made changes, delete.
			sprintf(command, "\\rm ./%s", myFilename);	// remove backup file
			system(command);
		}

		sprintf(myFilename, "%s", closedFile);		// set local data structures
		sprintf(_myFilename, "%s", closedFile);

		sendStatusMessage();
	}


}

/*
 * The giant while(true) loop for the server. Constantly listens to the UDP
 * multicast port for messages. Sends received packets off for processing.
 */


void play()	{
	while( true )
	{
		Sockaddr cliaddr;
		socklen_t len = sizeof(Sockaddr);
		int n;
		char buf[1024];

		bzero(buf, sizeof(buf));
		void processCompleteMessage(char * packet) {
			int c;
			char * command;

		//	7_<Commit #>_<complete>
			strtok(packet, "_");			// strip event type
			c = atoi(strtok(NULL, "_"));	// get commit seq number
			command = strtok(NULL, "_");	// get the COMMIT/ROLLBACK command

			if( c == myCommits ) {			// ignore old messages
				if( strcmp(command, "COMMIT") == 0) {
					commitMode = true;
				} else {
					commitMode = false;
				}

				localCommit();					// save the changes, or rollback file to backup
				sendDoneMessage();
				cleanupAfterCommit();
			} else {
				sendDoneMessage();
			}

		}
		// listen to port
		n = recvfrom(mySocket, buf, sizeof(buf), 0, (struct sockaddr *)&cliaddr, &len);
		if (n < 0 && errno != EINTR) {
			perror("event recvfrom");
			continue;
		} else {
			processPacket(buf);
		}

	}
}


void parseArguments(int argc, char* argv[]) {
	int i;
	/* Parse arguments */
	for (i = 1; i < argc; i += 2) {
		if (strcmp(argv[i], "-port") == 0)
			port = atoi(argv[i + 1]);
		else if (strcmp(argv[i], "-mount") == 0)
			mount = argv[i + 1];
		else if (strcmp(argv[i], "-drop") == 0)
			drop = atoi(argv[i + 1]);
		else {
			break;
		}
	}

	/* Error checks */
	if (port < 0 || drop < 0 || drop > 100 || mount == NULL) {
		printf("usage: %s "
				"-port <port> "
				"-mount <file path> "
				"-drop <drop rate>\n", argv[0]);
		exit(1);
	}
	//printf("INFO: Port %d, Mount %s, Drop %d\n", port, mount, drop);
}

int main(int argc, char *argv[]) {
	parseArguments(argc, argv);	// set the port, mount directory, and drop rate
	localInit();				// initialize local data structures
	directoryInit(mount);		// create mount directory
	netInit(port);				// set up the multicasting port
	play(); 					// set up the play loop

	return 0;
}

