/*
 * network.c
 *
 *  Created on: May 15, 2013
 *      Author: jessho
 */

#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <assert.h>
#include <sys/socket.h>
#include <bits/socket.h>
#include <arpa/inet.h>
#include <network.h>


/*
 * Resolve the specified host name into an internet address.  The "name" may
 * be either a character string name, or an address in the form a.b.c.d where
 * the pieces are octal, decimal, or hex numbers.  Returns a pointer to a
 * sockaddr_in struct (note this structure is statically allocated and must
 * be copied), or NULL if the name is unknown.
 */

/* Open and bind to multicasting UDP port. If port is 0, then we use the default
 * value PORT.
 */
void netInit(int port) {
	Sockaddr		nullAddr;
	int				reuse;
	u_char          ttl;
	struct ip_mreq  mreq;

//	bcopy((caddr_t) thisHost, (caddr_t) myAddrPtr, sizeof(Sockaddr));

	/* Create a socket */
	if ((mySocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("Socket");
	}

	/* SO_REUSEADDR allows more than one binding to the same
	   socket - you cannot have more than one server instance on one
	   machine without this */
	reuse = 1;
	if (setsockopt(mySocket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0) {
		perror("Setsockopt failed (SO_REUSEADDR)");
	}

	/* Bind the socket */
	nullAddr.sin_family = AF_INET;
	nullAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (port <= 0) {
		port = PORT; // use default value
	}
	nullAddr.sin_port = htons(port);
	if (bind(mySocket, (struct sockaddr *)&nullAddr, sizeof(nullAddr)) < 0) {
		perror("Socket bind");
	}

	/* Multicast TTL (time to live):
	   0 restricted to the same host
	   1 restricted to the same subnet
	   32 restricted to the same site
	   64 restricted to the same region
	   128 restricted to the same continent
	   255 unrestricted

	   DO NOT use a value > 32. If possible, use a value of 1 when testing.
	*/
	ttl = 1;
	if (setsockopt(mySocket, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl)) < 0) {
		perror("setsockopt failed (IP_MULTICAST_TTL)");
	}

	/* join the multicast group */
	mreq.imr_multiaddr.s_addr = htonl(GROUP);
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	if (setsockopt(mySocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *) &mreq, sizeof(mreq)) < 0) {
		perror("setsockopt failed (IP_ADD_MEMBERSHIP)");
	}

	/* Get the multi-cast address ready to use in sendto() calls. */
	memcpy(&groupAddr, &nullAddr, sizeof(Sockaddr));
	groupAddr.sin_addr.s_addr = htonl(GROUP);

	printf("INFO: joined multicast %x, port %d\n", groupAddr.sin_addr.s_addr, port);
}

void sendPacket(char * message) {
	int messageLen;

	if ((messageLen = sendto(mySocket, message, strlen(message), 0, (struct sockaddr *) &groupAddr, sizeof(Sockaddr))) < 0) {
		perror("Sendto error");
	} else {
		printf("INFO: %d bytes of %s\n", messageLen, message);
	}
}
