/****************/
/* Jessica Ho	*/
/* May 17, 2013	*/
/* CS 244B		*/
/* Spring 2013	*/
/****************/
#ifndef APPL_H_
#define APPL_H_

int const MaxWrites = 128;
int const MaxBlockLength = 512;	// in bytes

/* A unique port number will be assigned to you by your TA */
int const ReplFsPort = 40038;

int NormalExit = 0;
int ErrorExit = -1;


/* ------------------------------------------------------------------ */


#endif /* APPL_H_ */
