/****************/
/* Jessica Ho	  */
/* May 17, 2013	*/
/* CS 244B	    */
/* Spring 2013	*/
/****************/

//#define DEBUG true

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <assert.h>
#include <sys/socket.h>
#include <poll.h>
#include <string.h>

#include <client.h>
#include <network.h>



int NormalReturn = 0;
int ErrorReturn = -1;
const char *closedFile = "/";

int drop;		// percent of packets to drop (0 to 100)
int quorum;		// min number of servers that need to agree for client to continue
int tally;		// count of servers who've voted
int tries;		// count of tries before timing out
int myfd;			// file descriptor for original file
int _myfd;		// file descriptor for working file
char myFilename[MAXNAME];	// name of the original file
char _myFilename[MAXNAME+1];	// name of working file
int myCommits;	// current commit sequence
int myWrites;		// current write sequence
char messageBuffer[1024];	// buffer to hold incoming packets
bool votingMode;	// true when we are in phase 1 of committing. false otherwise.
bool commitMode;	// true when we can COMMIT. false when we ROLLBACK.

wupdate myHistory[MAXWRITES];	// array of pointers to write updates
server servers[MAXSERVERS];	// array of pointers to servers in replicated file system

/*
 * Given a packet, interprets it and routes control flow if a reaction
 * is necessary
 */
void processPacket(char * packet) {
	// temporarily add a delay to test that the port is set up properly
	//usleep(1000000);

	/*Depending on what type of message we've received from the network, we will
	 * update our internal state representation of other players, we will consider
	 * a tag claim, we will update our score based on a tag response, or we will
	 * remove a player who has quit.
	 * */
	if (rand()%100 < drop) return;
	int c = atoi(&packet[0]);
	switch(c) {
	case 1:	// status message from servers
//		printf("GOT status: %s\n", packet);
		processStatusMessage(packet);
		break;
	case 2:	// init message from client
		break;
	case 3:	// open message from client
		break;
	case 4:	// write message from client
//		printf("Heard write: %s\n", packet);
		break;
	case 5:	// poll message from client
		break;
	case 6:	// vote message from servers
//		printf("GOT: vote message: %s\n", packet);
		processVoteMessage(packet);
		break;
	case 7:	// complete message from client
		break;
	case 8:	// done (ACK) from server
//		printf("GOT: done message: %s\n", packet);
		processDoneMessage(packet);
		break;
	case 9:	// close message from client
		break;
	case 10:	// exit message from server
//		printf("GOT: exit message: %s\n", packet);
//		processExitMessage(packet);
		break;
	default:
		break;
	}
}

/*
 * Initializes local data structures ands fields, such as commits, writes,
 * servers, write histories, etc.
 */
void localInit(int packetLoss, int numServers) {
	drop = packetLoss;
	quorum = numServers;
	myfd = 0;				// file descriptor for file being edited
	_myfd = 0;				// file descriptor for backup version of file
	strcpy(myFilename, closedFile);	// name of the file being written
	myCommits = 0;			// current commit sequence
	myWrites = 0;			// current write sequence
	votingMode = false;		// voting == true when in 2-phase commit.
	commitMode = false;		// commiting == true when ready to finish transaction
	memset(myHistory, 0, sizeof(myHistory));	// clear the write history
	memset(servers, 0, sizeof(servers));		// clear the list of servers
	printf("Number of live servers: %d\n", numLiveServers());
}

/*
 *
 */
void sendInitMessage() {
	int bytes;
	char *ping = "2_0_0";
	if ((bytes = sendto(mySocket, ping, strlen(ping), 0, (struct sockaddr *) &groupAddr, sizeof(Sockaddr))) < 0) {
		perror("sendto(ping)");
	}
	printf("SENT init: %s\n", ping);
}

/*
 * Sends initial ping to servers listening on multicast. Listens for
 * quorum of servers. 1 second timeout before repinging. Allowed 5
 * attempts to for quorum.
 */
int quorumInit() {

	printf("INFO: joining...checking quorum.\n");

	return requestQuorum(sendInitMessage, numLiveServers);
}

/*
 * request quorum by sending the PING, and retrying the PING
 * until COUNTER observes that a quorum of servers has achieved
 * the desired property.
 */
int requestQuorum(void (*ping)(void), int (*counter)(void)) {
	int ret;
	tries = 1;
	int count = 0;
	struct pollfd fds[1];
	fds[0].fd = mySocket;
	fds[0].events = POLLIN | POLLPRI;
	int timeout = 250; // milliseconds
	// send the message
	(* ping)();
	// now wait for ACKs
	while (tries <= 5 && count < quorum) {
		/* Call poll(). timeout set for 1 second */
		ret = poll(fds, 1, timeout);

		if (ret < 0) {		// error
			perror("poll");
			return -1;
		}
		if (ret == 0) {		// timeout
			//printf("Failed on try %d\n", tries);
			tries++;
			(* ping)();
		}
		if (ret > 0) {		// there's an event at socket

			/* Regardless of requested events, poll() can always return these */
			if (fds[0].revents & (POLLERR | POLLHUP | POLLNVAL)) {
				perror("poll return events");
				return -1;
			}

			/* Check if data to read from socket */
			if (fds[0].revents & (POLLIN | POLLPRI)) {
				/* wipe buffer, since recvfrom writes to same buffer over and over */
				memset(messageBuffer, 0, sizeof(messageBuffer));

				if(recvfrom(mySocket, messageBuffer, sizeof(messageBuffer), 0, NULL, NULL) < 0) {
					perror("event recvfrom");
					return -1;
				}
			}

			processPacket(messageBuffer);
			if(count < (* counter)() ) {
				count = (* counter)();
			}
		}
	}
	return count;
}

void parseStatusMessage(server *s, char *packet) {
	// 1_<Server UUID>_<status seq #>_<commit seq #>_<write seq #>_<file name>
	strtok(packet, "_");								// strip event type
	sprintf(s->UID, "%s", strtok(NULL, "_"));	// get server UID
	s->status = atoi(strtok(NULL, "_"));				// get status seq number
	s->commit = atoi(strtok(NULL, "_"));				// getcommit seq number
	s->write = atoi(strtok(NULL, "_"));					// get write seq number
	sprintf(s->filename, "%s", strtok(NULL, "_")); 		// get the file name

//	printf("PARSED: 1, UID:%s, status:%d, commit:%d, write:%d, file:%s\n",
//	s->UID, s->status, s->commit, s->write, s->filename);
}

/*
 * Parses the string, packet, which contains a status message.
 * Updates local data structures if necessary
 */
void processStatusMessage(char * packet) {
	server s;
	s.alive = true;
	s.vote[0] = '\0';
	s.commitACK = false;

	parseStatusMessage(&s, packet);

	if(isServerNew(s.UID)) {
		addNewServer(&s);
		return;
	}
	if(isServerStatusNew(s.UID, s.status)) {
		updateServerInfo(&s);
		// in voting mode, need to send servers the missing packets
		if(votingMode) {
			sendWriteMessage(s.write);
			tries = 1;
		}
	}

}

/*
 * Loops through list of servers and counts how many spots are filled
 */
int numLiveServers() {
	int i;
	int count = 0;
	for(i = 0; i < MAXSERVERS; i++) {
		if(servers[i].alive) {
			count++;
		}
	}
	return count;
}

/*
 * Loops through list of servers we know about. If any UID matches, then
 * it's alreayd on record. If we reach an empty spot, then we found no matches.
 * If we reach the end of the list, there's no space for the new server.
 */
bool isServerNew(char * UID) {
	int i;
	for(i = 0; i < MAXSERVERS; i++) {
		if (!(servers[i].alive)) {
			return true;
		}
		if (strcmp(UID, servers[i].UID) == 0) {
			return false;
		}
	}
	return false;
}

/*
 * Loops through list of servers we know about, as soon as we find an empty
 * spot we fill it.
 */
void addNewServer(server *newServer) {
	int i = 0;
	while(servers[i].alive && i < MAXSERVERS) {
		i++;
	}
	if (i < MAXSERVERS) {
		memcpy(&servers[i], newServer, sizeof(server));
		printf("New server: Alive:%d UID:%s, commit:%d, write:%d, status:%d, file:%s, vote:%s, ACK:%d\n",
				servers[i].alive, servers[i].UID, servers[i].commit,
				servers[i].write, servers[i].status, servers[i].filename,
				servers[i].vote, servers[i].commitACK);
	}
}

/*
 * Given a UID, returns a server in our internal list.
 * If no match is found, return NULL.
 */
server *getServerByUID(char * UID) {
	int i = 0;
	while(i < MAXSERVERS && strcmp(servers[i].UID, UID) != 0) {
		i++;
	}
	if(i < MAXSERVERS) {
		return &servers[i];
	} else {
		return NULL;
	}
}

/*
 * Given a server's UID and a status sequence number, checks if the status
 * sequence number represents stale information.
 */
bool isServerStatusNew(char *UID, int status) {
	server *s = getServerByUID(UID);
	if(s->status >= status) {
		return false;
	} else {
		return true;
	}
}

/*
 * Given new version of server s, we update s's information in our internal
 * list. We must already know of s for this update to perform correctly.
 */
void updateServerInfo(server *s) {
	int i;
	while(i < MAXSERVERS && strcmp(s->UID, servers[i].UID) != 0) {
		i++;
	}
	if(i < MAXSERVERS) {
		memcpy(&servers[i], s, sizeof(server));
//		printf("Updated server. UID:%s, commit:%d, write:%d, status:%d, file:%s, vote:%s, ACK:%d\n",
//						servers[i].UID, servers[i].commit, servers[i].write,
//						servers[i].status, servers[i].filename, servers[i].vote,
//						servers[i].commitACK);
		printf("Updating server id:%s, status:%d, filename:%s\n", s->UID, s->status, s->filename);
	}
}

/*
 * Client sends a message to servers to open a the current working file.
 * Client expects ACKs.
 */
void sendOpenMessage(){
	int bytes;
	char open[256];
	sprintf(open, "3_%d_%d_%s", myCommits, myWrites, myFilename);
	if ((bytes = sendto(mySocket, open, strlen(open), 0, (struct sockaddr *) &groupAddr, sizeof(Sockaddr))) < 0) {
		perror("sendto(open)");
	}
//	printf("SENT open: %s\n", open);
}

/*
 * Counts number of servers in local state that indicate successfully opening
 * the current working file.
 */
int numOpenSuccessServers() {

	int i;
	int count = 0;
	for(i = 0; i < MAXSERVERS; i++) {
		if(servers[i].alive) {
			server s = servers[i];
			if(s.commit == myCommits && s.write == myWrites && strcmp(s.filename, myFilename) == 0) {
				count++;
			}
		}
	}
	return count;
}

int numCloseSuccessServers() {
	int i;
	int count = 0;
	for(i = 0; i < MAXSERVERS; i++) {
		if(servers[i].alive) {
			server s = servers[i];
			if(s.commit == myCommits && strcmp(s.filename, closedFile) == 0) {
				count++;
			}
		}
	}
//	printf("Num Servers closed file: %d\n", count);
	return count;
}

/*
 * Finds the first open position in the update history and fills it.
 */
void updateHistory(int commit, int write, char * name, char * block, int offset, int blockSize ){
	int i = myWrites - 1;
	myHistory[i].commit = commit;
	myHistory[i].index = write;
	sprintf(myHistory[i].filename, "%s", name);
	sprintf(myHistory[i].block, "%s", block);
	myHistory[i].offset = offset;
	myHistory[i].blockSize = blockSize;

//	printf("SENT write history. commit:%d, index:%d, blockSize:%d, offset:%d, block:%s, file:%s\n",
//			myHistory[i].commit, myHistory[i].index, myHistory[i].blockSize,
//			myHistory[i].offset, myHistory[i].block, myHistory[i].filename);
}
/*
 * When client has successfully written to a local file, it sends a
 * message to listening servers. When a server is requesting missing info,
 * the client sends the missing info to listening servers.
 */
void sendWriteMessage(int writeIndex){
	int bytes;
	char write[1024];
	wupdate w = myHistory[writeIndex];
//	4_<commit seq #>_<write seq #>_<block size>_<offset>_<write block>

	sprintf(write, "4_%d_%d_%d_%d_%s%c", w.commit, w.index, w.blockSize, w.offset, w.block, '!');
	if ((bytes = sendto(mySocket, write, strlen(write), 0, (struct sockaddr *) &groupAddr, sizeof(Sockaddr))) < 0) {
		perror("sendto(write)");
	}
//	printf("SENT write: %s\n", write);
}

/*
 * When committing, the first phase requires client to poll the servers and see
 * if any write updates need retransmission
 */
void sendPollMessage() {
	int bytes;
	char poll[100];

	//5_<commit seq #>_<write seq #>
	sprintf(poll, "5_%d_%d", myCommits, myWrites);
	if ((bytes = sendto(mySocket, poll, strlen(poll), 0, (struct sockaddr *) &groupAddr, sizeof(Sockaddr))) < 0) {
		perror("sendto(poll)");
	}
//	printf("SENT poll: %s\n", poll);
}

/*
 * Servers respond to a client's request for a vote. This function checks
 * how many servers have voted.
 */
int numVotedServers() {
	int i;
	int count = 0;
	for(i = 0; i < MAXSERVERS; i++) {
		if(servers[i].alive) {
			server s = servers[i];
			if(strlen(s.vote) > 0) {	// vote can be yes, no or empty string
				count++;
			}
		}
	}
	return count;
}

/*
 * Servers send votes (YES or NO) in response to client's poll request.
 * ProcessVoteMessage parses the message and, if needed, updates server's info.
 */
void processVoteMessage(char * packet) {
	server *s;
	int c;
	int w;

//	6_<server UID>_<commit seq #>_<write seq #>_<vote>

	strtok(packet, "_");			// strip event type
	s = getServerByUID(strtok(NULL, "_"));	// get *server by ID

	if( s == NULL ) {				// no matching server found
		return;
	}
	c = atoi(strtok(NULL, "_"));	// get commit seq number
	if( c < myCommits ) {			// ignore old messages
		return;
	}

	w = atoi(strtok(NULL, "_"));	// get write seq number
	if( w < myWrites) {				// ignore invalid votes (wrong write number)
		return;
	}

	sprintf(s->vote, "%s", strtok(NULL, "_"));	// get server's vote

	// printf("PARSED VOTE: UID:%s, commit:%d, write:%d, vote:%s\n", s->UID, s->commit, s->write, s->vote);
}

/*
 * After phase 1 of committing, client sends the message to initiate phase 2,
 * either commit or rollback.
 */
void sendCompleteMessage() {
	int bytes;
	char complete[64];
	char * command;
	if (commitMode) {
		command = "COMMIT";
	} else {
		command = "ROLLBACK";
	}

//	7_<Commit #>_<complete>
	sprintf(complete, "7_%d_%s", myCommits, command);
	if ((bytes = sendto(mySocket, complete, strlen(complete), 0, (struct sockaddr *) &groupAddr, sizeof(Sockaddr))) < 0) {
		perror("sendto(complete)");
	}
//	printf("SENT complete: %s\n", complete);
	return;
}

/*
 * Counts the number of servers that have ACK'd the Complete message
 */
int numDoneServers() {

	int i;
	int count = 0;
	for(i = 0; i < MAXSERVERS; i++) {
		// if server is alive and commit acknowledged
		if(servers[i].alive && servers[i].commitACK) {
			count++;
		}
	}
	//printf("Servers that have committed: %d\n", count);
	return count;
}

/*
 * Servers send ACK's in response to Complete Message.
 */
void processDoneMessage(char * packet) {
	server *s;
	int c;

//	8_<Server UUID>_<Commit #>

	strtok(packet, "_");			// strip event type
	s = getServerByUID(strtok(NULL, "_"));	// get server by ID

	c = atoi(strtok(NULL, "_"));	// get commit seq number
	if( c < myCommits ) {			// ignore old messages
		return;
	}

	s->commitACK = true;

//	printf("PARSED DONE: UID:%s, commit:%d\n", s->UID, s->commit);
}

/* ------------------------------------------------------------------ */
/* InitReplFs informs your system which UDP port # it is to use.
 * It is also given the percentage (0 to 100) of packets that should
 * be randomly dropped. numServers specifies the number of servers
 * that must be present in order to successfully commit changes to a
 * file.
 * Return value: 0 (NormalReturn)
 * Return value: -1 on failure (ErrorReturn). MAY be returned if a
 * server is unavailable.
 */
int InitReplFs( unsigned short portNum, int packetLoss, int numServers ) {
	printf("InitReplFs: Port number %d, packet loss %d percent, %d servers\n",
			portNum, packetLoss, numServers);
	/****************************************************/
	/* Initialize network access, local state, etc.     */
	/****************************************************/

	localInit(packetLoss, numServers);	// local state initialization
	netInit(portNum);					// network initialization
	int serversFound = quorumInit();	// check for quorum
	if (serversFound < quorum) {
		fprintf(stderr, "Quorum not achieved in IniReplFs: %d servers found.\n", serversFound);
		return( ErrorReturn );
	}

	return( NormalReturn );
}

/*
 * Check if a file exists before we open it.
 */
int file_exists (char * fileName)
{
   struct stat buf;
   int i = stat ( fileName, &buf );
     /* File found */
     if ( i == 0 )
     {
       return 1;
     }
     return 0;

}

/*
 * Controls locally opening and creating backup of filename
 */
int localOpenFile(char* fileName) {
	// make backup of original. set ~filename as working version

	myfd = open(fileName, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if (myfd < 0)
		perror("OpenFile");

	char command[300];
	sprintf(_myFilename, "%s~", fileName);	// set working filename
	sprintf(command, "\\cp ./%s ./%s", fileName, _myFilename);	// create working file from backup
	system(command);

	// close original
	close(myfd);

	// open ~filename
	_myfd = open(_myFilename, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if (_myfd < 0)
		perror("OpenFile");

	// set myFilename after all error checks
	sprintf(myFilename, "%s", fileName);

	return _myfd;
}

void sendCloseMessage(){
	int bytes;
	char message[256];
//	9_<commit seq #>_<transaction seq #>_<file name>
	sprintf(message, "9_%d_%d_%s", myCommits, myWrites, myFilename);
	if ((bytes = sendto(mySocket, message, strlen(message), 0, (struct sockaddr *) &groupAddr, sizeof(Sockaddr))) < 0) {
		perror("sendto(close)");
	}
//	printf("SENT close: %s\n", message);
}

/* ------------------------------------------------------------------ */
/* OpenFile takes the name of a file and returns a file descriptor or
 * error code. Server(s) will not truncate existing files when reopened.
 * OpenFile only supports flat names (no slashes). For now, OpenFile only
 * supports names up to 127 bytes in length, including the null terminator.
 * Server(s) open this exact file relative to your mount directory
 * (specified at server startup).
 *
 * Return value: a file descriptor >0 on success.
 * Return value: -1 on failure (ErrorReturn). MAY be returned if a server
 * is unavailable. It MAY also be returned if the implementation can only
 * handle one open file at a time and the application is attempting to open
 * a second. */
int OpenFile( char * fileName ) {

	ASSERT( fileName );
	printf("OpenFile: Opening File '%s'\n", fileName);

	// error checks
	if (strcmp(myFilename, closedFile) != 0) { // there's already a file open
		return (ErrorReturn);
	}
	if (strlen(fileName) + 1 > MAXNAME) { // filename too long
		return (ErrorReturn);
	}

	// make backup of original. set ~filename as working version. open.
	_myfd = localOpenFile(fileName);

	// adjust the counters
	myCommits++;
	myWrites = 0;

	// get quorum
	int q = requestQuorum(sendOpenMessage, numOpenSuccessServers);
	if(quorum > q) {
		printf("Quorum not achieved in OpenFile: %d servers found\n", q);
		return (ErrorReturn);
	}

	// let clients manipulate the working version
	return( _myfd );
}

/* ------------------------------------------------------------------ */
/* WriteBlock stages a contiguous chunk of data to be written upon Commit().
 * fd is the file descriptor of the file to write to, as returned by a
 * previous call to OpenFile. buffer is a pointer to the data to be written,
 * and blockSize is the number of bytes in buffer. byteOffset is the offset
 * within the file to write to.
 *
 * Return value: The number of bytes staged (blockSize).
 * Return value: -1 (ErrorReturn) returned if the file descriptor is invalid.
 * MAY be returned if the client violates the maximum block size (512 MB),
 * file size (1 MB ==  2^20 bytes), or number of staged writes (128) before
 * Commit(). WILL NOT be returned if a server is unavailable. */
int WriteBlock( int fd, char * buffer, int byteOffset, int blockSize ) {
	//char strError[64];
	int bytesWritten;

	ASSERT( fd >= 0 );
	ASSERT( byteOffset >= 0 );
	ASSERT( buffer );
	ASSERT( blockSize >= 0 && blockSize < MAXBLOCK );

	#ifdef DEBUG
	printf( "WriteBlock: Writing FD=%d, Offset=%d, Length=%d\n", fd, byteOffset, blockSize );
	#endif

	if(myWrites >= 128) {
		perror("WriteBlock: updates between commits is capped to 128.\n");
		return(ErrorReturn);
	}
	if(blockSize > 512) {
		perror("WriteBlock: Max block size is 512 bytes.\n");
	}
	if ( lseek( fd, byteOffset, SEEK_SET ) < 0 ) {
		perror( "WriteBlock Seek" );
		return(ErrorReturn);
	}

	if ( ( bytesWritten = write( fd, buffer, blockSize ) ) < 0 ) {
		perror( "WriteBlock write" );
		return(ErrorReturn);
	} else {
		myWrites++;
		//printf("INFO offset:%d, file:%s, %d bytes of '%s'\n", byteOffset, myFilename, bytesWritten, buffer);
	}

	updateHistory(myCommits, myWrites, myFilename, buffer, byteOffset, blockSize);

	sendWriteMessage(myWrites-1);

	return( bytesWritten );

}

/*
 * Count number of servers with a particular VOTE.
 */
int countVotes(char *vote) {
	int i;
	int count = 0;
	for(i = 0; i < MAXSERVERS; i++) {
		if(servers[i].alive && strcmp(servers[i].vote, vote) == 0) {
			count++;
		}
	}
//	printf("Servers that have voted %s: %d\n", vote, count);
	return count;
}

/*
 * After a commit/rollback, update counters and reset local data structures.
 */
void cleanupAfterCommit() {
	myCommits++; // update local data
	myWrites = 0;
	memset(myHistory, 0, sizeof(myHistory));	// wipe write history
	votingMode = false;
	commitMode = false;
	// wipe server votes and ACKs
	int i;
	for( i = 0; i < MAXSERVERS; i++) {
		servers[i].vote[0] = '\0';
		servers[i].commitACK = false;
	}
}

/*
 * Takes care of copying or rolling back the working file version to the
 * backup version.
 */
void localCommit() {
	char command[300];
	if (commitMode) {
		// commit locally by copying working to original file
		sprintf(command, "\\cp ./%s ./%s", _myFilename, myFilename);
		system(command);
	} else {
		// rollback by coping original to working file
		sprintf(command, "\\cp ./%s ./%s", myFilename, _myFilename);
		system(command);
	}
}

/* ------------------------------------------------------------------ */
/* Commit takes a file descriptor and commits all writes made via WriteBlock()
 * since the last Commit() or Abort().
 *
 * Return value: 0 (NormalReturn) on success (i.e., changes made were
 * committed).
 * Return value: -1 (ErrorReturn) if the client had outstanding changes and
 * a server is unavailable (so the changes could not be committed). It MAY
 * be returned if the client had no outstanding changes and a server is
 * unavailable. */
int Commit( int fd ) {
	ASSERT( fd >= 0 );
	printf("Commit: FD=%d\n", fd);

	if (myWrites == 0) {	// no-op if no changes
		return (NormalReturn);
	}

	/****************************************************/
	/* Prepare to Commit Phase			    */
	/* - Check that all writes made it to the server(s) */
	/****************************************************/
	// Step 1: give servers a little time to finish staged writes
	usleep(500000); // units: microseconds
	// Step 2: poll the servers
	votingMode = true;
	int q = requestQuorum(sendPollMessage, numVotedServers);
	if (quorum > q) {
		printf("Failed to achieve quorum on vote phase of commit: %d servers found\n", q);
		return (ErrorReturn);
	}
	votingMode = false;

	/****************/
	/* Commit Phase */
	/****************/
	// Step 1: count the YES and NO votes, and send completion notice
	if (countVotes("NO") > 0) { // rollback
		commitMode = false;
	} else if (quorum <= countVotes("YES")) { // commit
		commitMode = true;
	} else {
		commitMode = false;
	}

	localCommit();	// local file saving

	q = requestQuorum(sendCompleteMessage, numDoneServers);
	// Step 2: get ACKs of completion
	if (quorum < q) {
		printf("Failed to achieve quorum on committing phase of commit: %d servers found\n", q);
		return (ErrorReturn);
	}

	cleanupAfterCommit(); // update local data and counters

	return( NormalReturn );

}

/* ------------------------------------------------------------------ */
/* Abort takes a file descriptor and discards all changes since the last commit.
 *
 * Return value: 0 (NormalReturn).
 * Return value: -1 (ErrorReturn) may be returned if the file descriptor is
 * invalid.  */
int Abort( int fd )
{
	ASSERT( fd >= 0 );

	#ifdef DEBUG
	printf( "Abort: FD=%d\n", fd );
	#endif

	/*************************/
	/* Abort the transaction */
	/*************************/
	commitMode = false;

	localCommit();	// local file saving

	// Step 2: get ACKs of completion
	int q = requestQuorum(sendCompleteMessage, numDoneServers);
	if (quorum < q) {
		printf("Failed to reach quorum on Abort: %d servers found\n", q);
		return (ErrorReturn);
	}

	cleanupAfterCommit(); // update local data and counters

	return(NormalReturn);
}

/* ------------------------------------------------------------------ */
/* CloseFile relinquishes all control that the client had with the file.
 * Close() also attempts to commit all changes that have not been saved.
 *
 * Return values: same as in Commit(). */
int CloseFile( int fd ) {

	ASSERT( fd >= 0 );

	#ifdef DEBUG
	printf( "Close: FD=%d\n", fd );
	#endif

	/*****************************/
	/* Check for Commit or Abort */
	/*****************************/

	Commit(fd);						// call commit on any unsaved changes

	if ( close( fd ) < 0 ) {		// this will close the _myfd
		perror("Close");
		return(ErrorReturn);
	}

	char command[300];
	sprintf(command, "\\rm ./%s", _myFilename);	// remove working file
	system(command);

	int q = requestQuorum(sendCloseMessage, numCloseSuccessServers);
	if(quorum > q) {
		printf("Failed to achieve quorum on Close: %d servers found.\n", q);
		return(ErrorReturn);
	}

	sprintf(myFilename, "%s", closedFile);		// set local data structures
	sprintf(_myFilename, "%s", closedFile);

	return(NormalReturn);
}

/* ------------------------------------------------------------------ */




