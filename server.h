/*
 * server.h
 *
 *  Created on: May 14, 2013
 *      Author: jessho
 */

#ifndef SERVER_H_
#define SERVER_H_

/*
 * Some definitions and global constants
 */
#define MAXWRITES 128		// Max writes per commit
#define MAXBLOCK 512		// Max write block in bytes
double const MAXFILE = 2e20;	// Max file size in bytes
#define MAXNAME 127		// Max file name size in bytes




/*
 * Debugging assistance. To enter debug mode, uncomment #define ASSERT_DEBUG.
 */
// #define ASSERT_DEBUG true
#ifdef ASSERT_DEBUG
#define ASSERT(ASSERTION) \
 { assert(ASSERTION); }
#else
#define ASSERT(ASSERTION) \
{ }
#endif

/*
 * Function prototypes below
 */
#ifdef __cplusplus
extern "C" {
#endif

void generateUID();
void directoryInit(char * path);
void play();
void localInit();
void processPacket(char * packet);
void processInitMessage(char * packet);
void processOpenMessage(char * packet);
void processWriteMessage(char * packet);
void processPollMessage(char * packet);
void processCompleteMessage(char * packet);
void processDoneMessage(char * packet);
void processCloseMessage(char * packet);
void sendStatusMessage();
void sendVoteMessage();
void sendDoneMessage();
void sendExitMessage();
void updateHistory(int commit, int write, char * name, char * block, int offset, int blockSize);
void cleanupAfterCommit();
void tryWrite();
void localCommit();
int file_exists (char * fileName);

#ifdef __cplusplus
}
#endif

#endif /* SERVER_H_ */
