/*
 * network.h
 *
 *  Created on: May 15, 2013
 *      Author: jessho
 */

#ifndef NETWORK_H_
#define NETWORK_H_

#include <netinet/in.h>		// include this so we can use sockaddr_in data types

#define GROUP 	0xe0010101 	// multicast group 224.1.1.1
#define PORT	40038		// assigned by TA

int mySocket;
typedef	struct sockaddr_in	Sockaddr;
Sockaddr *myAddrPtr;
Sockaddr groupAddr;			// send packets for multicasting to this address

#ifdef __cplusplus
extern "C" {
#endif

void netInit(int port);
void sendPacket(char * message);

#ifdef __cplusplus
}
#endif

#endif /* NETWORK_H_ */
