/****************/
/* Jessica Ho	*/
/* May 17, 2013	*/
/* CS 244B		*/
/* Spring 2013	*/
/****************/

#ifndef CLIENT_H_
#define CLIENT_H_


/*
 * Debugging assistance. To enter debug mode, uncomment #define ASSERT_DEBUG
 */
// #define ASSERT_DEBUG true
#ifdef ASSERT_DEBUG
#define ASSERT(ASSERTION) \
 { assert(ASSERTION); }
#else
#define ASSERT(ASSERTION) \
{ }
#endif


/********************/
/* Client Functions */
/********************/

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>


// some variables, constants, and data structures
#define MAXSERVERS 6
#define MAXNAME 127
#define MAXWRITES 128
#define MAXBLOCK 512	// in bytes
#define TIMEOUT 1000	// in milliseconds


typedef	struct {
	int commit;				// write update commit #
	int index;				// write update sequence number
	char filename[MAXNAME];	// file name for write update
	char block[MAXBLOCK+1];	// content of the write update
	int offset;				// offset into file for writing
	int blockSize;			// size of the block
} wupdate;

typedef struct {
	bool alive;		// whether or not server is alive or dead
	char UID[37];	// server's unique ID number
	int status;		// server's latest status sequence number
	int commit;		// server's current commit sequence number
	int write;		// server's current write sequence number
	char filename[MAXNAME];	// filename of what server's writing
	char vote[4];	// server's vote during 2PC's voting phase (YES or NO)
	bool commitACK;	// server's ACK during 2PC's commit phase
} server;


/* InitReplFs gives the client filesystem a chance to perform any startup
 * tasks and informs your system which UDP port # it is to use. It is also
 * given the percentage (0 to 100) of packets that the client should
 * randomly drop (for testing), when they are received from the network.
 * Finally, numServers specifies the number of servers that must be present
 * in order to successfully commit changes to a file.
 *
 * Client system has undefined  behavior if InitReplFs is not called prior
 * to using the other calls.
 *
 * Return value: 0 (NormalReturn)
 * Return value: -1 on failure (ErrorReturn). MAY be returned if a
 * server is unavailable. */
extern int InitReplFs(unsigned short portNum, int packetLoss, int numServers);

/* OpenFile takes the name of a file and returns a file descriptor or
 * error code. Server(s) will not truncate existing files when reopened.
 * File names containing slashes are not handled. Names can be up to 127 bytes
 * in length, including the null terminator.
 *
 * Return value: a file descriptor >0 on success.
 * Return value: -1 on failure (ErrorReturn). MAY be returned if a server
 * is unavailable. It MAY also be returned if the implementation can only
 * handle one open file at a time and the application is attempting to
 * open a second.
*/
extern int OpenFile(char * strFileName);

/* WriteBlock stages a contiguous chunk of data to be written upon Commit().
 * fd is the file descriptor of the file to write to, as returned by a
 * previous call to OpenFile. buffer is a pointer to the data to be written,
 * and blockSize is the number of bytes in buffer. byteOffset is the offset
 * within the file to write to.
 *
 * Return value: The number of bytes staged (blockSize).
 * Return value: -1 (ErrorReturn) returned if the file descriptor is invalid.
 * MAY be returned if the client violates the maximum block size (512 MB),
 * file size (1 MB ==  2^20 bytes), or number of staged writes (128) before
 * Commit(). WILL NOT be returned if a server is unavailable. */
extern int WriteBlock(int fd, char * strData, int byteOffset, int blockSize);

/* Commit takes a file descriptor and commits all writes made via WriteBlock()
 * since the last Commit() or Abort().
 *
 * Return value: 0 (NormalReturn) on success (i.e., changes made were
 * committed).
 * Return value: -1 (ErrorReturn) if the client had outstanding changes and
 * a server is unavailable (so the changes could not be committed). It MAY
 * be returned if the client had no outstanding changes and a server is
 * unavailable. */
extern int Commit(int fd);

/* Abort takes a file descriptor and discards all changes since the last commit.
 *
 * Return value: 0 (NormalReturn).
 * Return value: -1 (ErrorReturn) may be returned if the file descriptor is
 * invalid.  */
extern int Abort(int fd);

/* CloseFile relinquishes all control that the client had with the file.
 * Close() also attempts to commit all changes that have not been saved.
 *
 * Return values: same as in Commit(). */
extern int CloseFile(int fd);

void processPacket(char * packet);
void processStatusMessage(char *packet);
void processVoteMessage(char *packet);
void processDoneMessage(char *packet);
void processExitMessage(char *packet);
void sendInitMessage();
void sendOpenMessage();
void sendWriteMessage();
void sendPollMessage();
void sendCompleteMessage();
void sendCloseMessage();

void localInit(int packetLoss, int numServers);
int quorumInit();
int requestQuorum(void (*ping)(void), int (*counter)(void));
bool isServerNew(char *serverID);
void addNewServer(server *newServer);
int numLiveServers();
int numOpenSuccessServers();
int numVotedServers();
int numDoneServers();
bool isServerStatusNew(char * UID, int status);
server *getServerByUID(char *UID);
void updateServerInfo(server *s);
void updateHistory(int commit, int write, char * name, char * block, int offset, int size);
int countVotes(char *vote);
void cleanupAfterCommit();
void localCommit();
int file_exists (char * fileName);
void parseStatusMessage(server *s, char *packet);


#ifdef __cplusplus
}
#endif

#endif /* CLIENT_H_ */
