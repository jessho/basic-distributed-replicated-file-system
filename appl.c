/****************/
/* Jessica Ho	  */
/* May 17, 2013	*/
/* CS 244B	    */
/* Spring 2013	*/
/****************/

#define DEBUG

#include <stdio.h>
#include <string.h>

#include <client.h>
#include <appl.h>

#define log printf
/* ------------------------------------------------------------------ */

static int
openFile(char *file)
{
  int fd = OpenFile(file);
  if (fd < 0) printf("OpenFile(%s): failed (%d)\n", file, fd);
  return fd;
}

static int
commit(int fd)
{
  int result = Commit(fd);
  if (result < 0) printf("Commit(%d): failed (%d)\n", fd, result);
  return fd;
}

static int
closeFile(int fd)
{
  int result = CloseFile(fd);
  if (result < 0) printf("CloseFile(%d): failed (%d)\n", fd, result);
  return fd;
}

static void
appl1() {
    // simple case - just commit a single write update.

    int fd;
    int retVal;

    fd = openFile( "file1" );
    retVal = WriteBlock( fd, "abcdefghijkl", 0, 12 );
    retVal = commit( fd );
    retVal = closeFile( fd );
}

static void
appl2() {

    // simple case - just commit a single write update using closeFile.

    int fd;
    int retVal;

    fd = openFile( "file2" );
    retVal = WriteBlock( fd, "abcdefghijkl", 0, 12 );
    retVal = closeFile( fd ); // should commit the changes
}

static void
appl3() {

    //  simple case - just abort a single update on a new file
    //  the file  should not be in the mount directory at the end given
    //  it was not existing to start with. Script should check that


    int fd;
    int retVal;

    fd = openFile( "file3" );
    retVal = WriteBlock( fd, "abcdefghijkl", 0, 12 );
    Abort( fd );
    retVal = closeFile( fd ); // should abort and remove the file
}

static void
appl4() {

    //  simple case - just do a single commit and then single abort
    //  the file should be in the  mount directory with the committed
    //  write only thus no "mno"in it


    int fd;
    int retVal;

    fd = openFile( "file4" );
    retVal = WriteBlock( fd, "abcdefghijkl", 0, 12 );
    retVal = commit( fd );
    retVal = WriteBlock( fd, "mno", 12, 3 );
    Abort( fd );
    retVal = closeFile( fd );
}

static void
appl5() {

    // checks simple overwrite case

    int fd;
    int retVal;

    fd = openFile( "file5" );
    retVal = WriteBlock( fd, "aecdefghijkl", 0, 12 );
    retVal = WriteBlock( fd, "b", 1, 1 );
    retVal = commit( fd );
    retVal = closeFile( fd );
}

static void
appl6() {
    // try out commit - abort with multiple writes
    // No XXX should be in the file

    int fd;
    int retVal;
    int i;
    char * commitStr = "deadbeef";
    char * abortStr = "XXX";

    int commitStrLength = strlen( commitStr );
    int abortStrLength = strlen( abortStr );


    fd = openFile( "file6" );
    for (i = 0; i < 29; i++)
        retVal = WriteBlock( fd, commitStr, i * commitStrLength ,
                             commitStrLength );
    retVal = commit( fd );

    for (i = 0; i < 7; i++)
        retVal = WriteBlock( fd, abortStr, i * abortStrLength ,
                             abortStrLength );
    Abort( fd );

    retVal = closeFile( fd );

}


static void
appl7() {
    // try out commit - abort - commit sequence
    // No XXX should be in the file

    int fd;
    int retVal;
    int i;
    char * commitStr = "deadbeef";
    char * abortStr = "XXX";
    char * commitStr2 = "1234567";

    int commitStrLength = strlen( commitStr );
    int abortStrLength = strlen( abortStr );
    int commitStr2Length = strlen( commitStr2 );

    fd = openFile( "file7" );
    for (i = 0; i < 29; i++)
        retVal = WriteBlock( fd, commitStr, i * commitStrLength ,
                             commitStrLength );
    retVal = commit( fd );

    for (i = 0; i < 7; i++)
        retVal = WriteBlock( fd, abortStr, i * abortStrLength ,
                             abortStrLength );
    Abort( fd );

    for (i = 29; i < 50; i++)
        retVal = WriteBlock( fd, commitStr2, i * commitStr2Length ,
                             commitStr2Length );

    retVal = commit( fd );
    retVal = closeFile( fd );

}

static void
appl8() {
    // multiple openFiles of the same file. As a consequence,
    // this also checks that
    // when a file exists in the mount directory, they should openFile it
    // and not create a new one.

	printf("got here 1\n");
    int fd;
    int retVal;
    int i;
    char commitStrBuf[512];

    for( i = 0; i < 512; i++ )
        commitStrBuf[i] = '1';

    printf("got here 2\n");
    fd = openFile( "file8" );

    // write first transaction starting at offset 512
    for (i = 0; i < 50; i++)
        retVal = WriteBlock( fd, commitStrBuf, 512 + i * 512 , 512 );

    printf("got here 3\n");
    retVal = commit( fd );
    retVal = closeFile( fd );

    for( i = 0; i < 512; i++ )
        commitStrBuf[i] = '2';

    fd = openFile( "file8" );

    // write second transaction starting at offset 0
    retVal = WriteBlock( fd, commitStrBuf, 0 , 512 );

    retVal = commit( fd );
    retVal = closeFile( fd );


    for( i = 0; i < 512; i++ )
        commitStrBuf[i] = '3';

    fd = openFile( "file8" );

    // write third transaction starting at offset 50*512
    for (i = 0; i < 100; i++)
        retVal = WriteBlock( fd, commitStrBuf, 50 * 512 + i * 512 , 512 );

    retVal = commit( fd );
    retVal = closeFile( fd );
}



static void
appl9() {
    // multiple aborts and then commits of the same file. As a consequence,
    // this also checks that
    // when a file exists in the mount directory, they should openFile it
    // and not create a new one.
    // also try out max. number of write updates to stress
    // No X, Y or Z should be in the file. The file should have
    // only '0' s and '%' at offset  100-200 and 1000000 - 1000300

    int fd;
    int retVal;
    int i;
    char commitStrBuf[512];

    for( i = 0; i < 512; i++ )
        commitStrBuf[i] = '0';

    fd = openFile( "file9" );

    // zero out a max. size file with max. number of writes
    // was 2048, now is 127
    for (i = 0; i < 127; i++)
        retVal = WriteBlock( fd, commitStrBuf, i * 512 , 512 );

    retVal = commit( fd );
    retVal = closeFile( fd );

    // now try bunch of aborts on the file.

    fd = openFile( "file9" );

    for (i = 0; i < 29; i++)
        retVal = WriteBlock( fd, "XXXXXXX", i * 200 , 7 );

    Abort( fd );
    retVal = closeFile( fd );


    fd = openFile( "file9" );

    for (i = 0; i < 59; i++)
        retVal = WriteBlock( fd, "YYYYYYY", 8888 + i * 200 , 7 );

    Abort( fd );
    retVal = closeFile( fd );


    fd = openFile( "file9" );

    for (i = 0; i < 109; i++)
        retVal = WriteBlock( fd, "ZZZZZ", 33333 + i * 100 , 5 );

    Abort( fd );
    retVal = closeFile( fd );

    log("Done aborting.\n");


    fd = openFile("file9");
    for (i = 0; i < 10; i++)
        retVal = WriteBlock( fd, "%%%%%%%%%%", 100 + i  * 10, 10 );
    retVal = commit(fd);
    retVal = closeFile(fd);


    fd = openFile("file9");
    for (i = 0; i < 60; i++)
        retVal = WriteBlock( fd, "%%%%%", 1000000 + i  * 5, 5 );
    retVal = commit(fd);
    retVal = closeFile(fd);

}


void
static appl10() {
    // test that if a server is crashed at write updates time,
    // the library aborts the transaction at commit time
    // the file should have only 0's in it.

    int fd;
    int retVal;
    int i;
    char commitStrBuf[512];

    for( i = 0; i < 256; i++ )
        commitStrBuf[i] = '0';

    fd = openFile( "file10" );

    // zero out the file first
    for (i = 0; i < 100; i++)
        retVal = WriteBlock( fd, commitStrBuf, i * 256 , 256 );

    retVal = commit( fd );
    retVal = closeFile( fd );

    fd = openFile( "file10" );
    retVal = WriteBlock( fd, "111111", 0 , 6 );

    // KILL ONE OF THE  SERVERS HERE BY ISSUING A SYSTEM CALL
    log("killing server\n");
   // stopServer(0);
    log("committing\n");

    retVal = commit( fd ); // this should return in abort
    retVal = closeFile( fd );
}



// Some lower priority error cases for coding sanity

static void
appl11() {

    // checks that a WriteBlock to a non-openFile file descriptor
    // is skipped
    // There should be only 12 0's at the end in the file

    int fd;
    int retVal;

    fd = openFile( "file11" );
    retVal = WriteBlock( fd, "000000000000", 0, 12 );

    // the following should not be performed due to wrong fd
    retVal = WriteBlock( fd + 1, "abcdefghijkl", 0, 12 );

    retVal = commit( fd );
    retVal = closeFile( fd );

}


static void
appl12() {

    // checks that funky order of operations does not cause crash or
    // bad behavior
    // there should be only 12 0's in the file

    int fd;
    int retVal;

    fd = openFile( "file12" );
    retVal = WriteBlock( fd, "000000000000", 0, 12 );
    retVal = closeFile( fd );

    // the following should not be performed due to file not openFile
    retVal = WriteBlock( fd, "abcdefghijkl", 0, 12 );
    retVal = commit( fd );
    retVal = closeFile( fd );

}

//Used for packetloss check
static void
appl13() {
    // simple case - just commit a single write update.

    int fd;
    int retVal;

    fd = openFile( "file13" );
    retVal = WriteBlock( fd, "abcdefghijkl", 0, 12 );
    retVal = commit( fd );
    retVal = closeFile( fd );
}

int
main() {

  int fd;
  int i;
  int byteOffset= 0;
  char strData[MaxBlockLength];

  char fileName[32] = "writeTest.txt";

  /*****************************/
  /* Initialize the system     */
  /*****************************/

  if( InitReplFs( ReplFsPort, 0, 4 ) < 0 ) {
    fprintf( stderr, "Error initializing the system\n" );
    return( ErrorExit );
  }

  /*****************************/
  /* Open the file for writing */
  /*****************************/

  fd = OpenFile( fileName );
  if ( fd < 0 ) {
    fprintf( stderr, "Error opening file '%s'\n", fileName );
    return( ErrorExit );
  }

  /**************************************/
  /* Write incrementing numbers to the file */
  /**************************************/

  for ( i=0; i<128; i++ ) {
    sprintf( strData, "%d\n", i );

#ifdef DEBUG
    printf( "%d: Writing '%s' to file.\n", i, strData );
#endif

    if ( WriteBlock( fd, strData, byteOffset, strlen( strData ) ) < 0 ) {
      printf( "Error writing to file %s [LoopCnt=%d]\n", fileName, i );
      return( ErrorExit );
    }
    byteOffset += strlen( strData );

  }


  /**********************************************/
  /* Can we commit the writes to the server(s)? */
  /**********************************************/
  if ( Commit( fd ) < 0 ) {
    printf( "Could not commit changes to File '%s'\n", fileName );
    return( ErrorExit );
  }

  /**************************************/
  /* Close the writes to the server(s) */
  /**************************************/
  if ( CloseFile( fd ) < 0 ) {
    printf( "Error Closing File '%s'\n", fileName );
    return( ErrorExit );
  }

  printf( "Writes to file '%s' complete.\n", fileName );

  // try the supplied test cases
  appl1();
  appl2();
  appl3();
  appl4();
  appl5();
  appl6();
  appl7();
  appl8();
  appl9();
  appl10();
  appl11();
  appl12();
  appl13();

  return( NormalExit );
}



/* ------------------------------------------------------------------ */
